package com.titapon.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldDeposittSuccess() {
        BookBank book = new BookBank("Name",0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100, book.balance, 0.00001);
    }


    @Test
    public void shouldDepositNagative() {
        BookBank book = new BookBank("Name",0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.balance, 0.00001);
    }

    @Test
    public void shouldWithDrawSuccess() {
        BookBank book = new BookBank("Name",0);
        book.deposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.balance, 0.00001);

    }

    @Test
    public void shouldWithDrawNegative() {
        BookBank book = new BookBank("Name",0);
        book.deposit(100);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100, book.balance, 0.00001);


    }


    @Test
    public void shouldWithDrawOverBalance() {
        BookBank book = new BookBank("Name",0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.balance, 0.00001);



    }


    @Test
    public void shouldWithDraw100Balance100() {
        BookBank book = new BookBank("Name",0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.balance, 0.00001);



    }
    




        
    
    
}
