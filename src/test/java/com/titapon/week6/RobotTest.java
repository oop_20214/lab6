package com.titapon.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess() {
        Robot robot = new Robot("Robot",'R',10,11);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());

    }

    @Test
    public void shouldCreateUpSuccess() {
        Robot robot = new Robot("Robot",'R',10,11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());

    }


    @Test
    public void shouldRobotUpFailAtMin() {
        Robot robot = new Robot("Robot",'R',10,Robot.Y_MIN);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY());

    }


    @Test
    public void shouldRobotDownSuccess() {
        Robot robot = new Robot("Robot",'R',10,9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    

    @Test
    public void shouldRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot",'R',10,Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }


    @Test
    public void shouldRobotDownAtMax() {
        Robot robot = new Robot("Robot",'R',10,Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }


    @Test
    public void shouldRobotLeftSccess() {
        Robot robot = new Robot("Robot",'R',10,9);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(9, robot.getX());
    }



    @Test
    public void shouldRobotLefFailMin() {
        Robot robot = new Robot("Robot",'R',Robot.X_MIN,9);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }


    @Test
    public void shouldRobotRightSccess() {
        Robot robot = new Robot("Robot",'R',10,9);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(9, robot.getY());
    }

    
    @Test
    public void shouldRobotRightMax() {
        Robot robot = new Robot("Robot",'R',Robot.X_MAX,9);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }

    



    
}
